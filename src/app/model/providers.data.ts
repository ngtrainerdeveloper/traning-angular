export var providers = [
    {
        "id": 414532,
        "firstname": "Marylinda",
        "lastname": "Bevir",
        "position": "Chief Executive Office",
        "company": {
            "company_name": "Jabbersphere",
            "address": "1081 Arapahoe Court",
            "city": "Houston",
            "state": "TX",
            "postal_code": "77234",
            "phone": "713-849-1712",
            "email": "mbevir@jabbersphere.com",
            "description": "Versatile Asynchronous Collaboration",
            "tagline": "Visualize Cross-Platform Action-Items"
        }
    },
    {
        "id": 631452,
        "firstname": "Perry",
        "lastname": "Jeanin",
        "position": "Product Engineer",
        "company": {
            "company_name": "Talane",
            "address": "969 Acker Lane",
            "city": "Jacksonville",
            "state": "FL",
            "postal_code": "32230",
            "phone": "904-326-4732",
            "email": "perry.jeanin@talane.com",
            "description": "Universal Impactful Hardware",
            "tagline": "Redefine Interactive Channels"
        }
    },
    {
        "id": 802323,
        "firstname": "Skipton",
        "lastname": "Duignan",
        "position": "Community Outreach Specialist",
        "company": {
            "company_name": "Browseblab",
            "address": "391 International Drive",
            "city": "Tucson",
            "state": "AZ",
            "postal_code": "85732",
            "phone": "520-476-3641",
            "email": "skiptonduignan@browseblab.com",
            "description": "Inverse Demand-Driven Archive",
            "tagline": "Synthesize Granular Infrastructures"
        }
    }
]