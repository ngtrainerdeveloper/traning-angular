import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProviderClass } from '../model/provider.model';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  private baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  // Get all providers
  getProviders(): Observable<ProviderClass[]> {
    return this.http.get<ProviderClass[]>(`${this.baseUrl}/`);
  }

  // Get one provider by ID
  getProviderById(id: number): Observable<ProviderClass> {
    return this.http.get<ProviderClass>(`${this.baseUrl}/${id}`);
  }

  // Create a new provider
  createProvider(provider: ProviderClass): Observable<any> {
    return this.http.post(`${this.baseUrl}/create-provider`, provider);
  }

  // Update an existing provider
  updateProvider(id: number, provider: ProviderClass): Observable<any> {
    return this.http.put(`${this.baseUrl}/${id}`, provider);
  }

  // Delete all providers
  deleteAllProviders(): Observable<any> {
    return this.http.delete(`${this.baseUrl}/deleteAll`);
  }

  // Delete one provider by ID
  deleteProviderById(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }
}
