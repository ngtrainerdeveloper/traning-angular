import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { ProvidersComponent } from './providers/providers.component';
import { RegisterComponent } from './register/register.component';
import { AddProvidersComponent } from './providers/add-providers/add-providers.component';
import { EditProvidersComponent } from './providers/edit-providers/edit-providers.component';
import { DetailsProvidersComponent } from './providers/details-providers/details-providers.component';
import { DeleteProviderComponent } from './providers/delete-provider/delete-provider.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "about", component: AboutComponent},
  {path: "contact", component: ContactComponent},
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "forgot-password", component: ForgotpasswordComponent},
  {
    path: "providers",
    children: [
      { path: "", component: ProvidersComponent },
      { path: "add", component: AddProvidersComponent },
      { path: "edit/:id", component: EditProvidersComponent },
      { path: "info/:id", component: DetailsProvidersComponent },
      { path: "delete/:id", component: DeleteProviderComponent }
    ]
  },
  {path: "**", component: NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
