import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProviderClass } from 'src/app/model/provider.model';
import { ProviderService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-details-providers',
  templateUrl: './details-providers.component.html',
  styles: []
})
export class DetailsProvidersComponent implements OnInit {

  id: number = 0;
  company: string = '';
  description: string = '';
  tagline: string = '';
  address: string = '';
  city: string = '';
  state: string = '';
  postal_code: string = '';
  phone: string = '';
  provider = new ProviderClass();
  providersForm!: FormGroup;
  
  constructor(private providerService: ProviderService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.valueOnPage;
  }

  get valueOnPage(){
    this.route.paramMap.subscribe(params => this.id = parseInt(params.get('id')!));
    return this.providerService.getProviderById(this.id).subscribe(
      data =>{
        this.provider = data as ProviderClass;
        console.log(data);
        if(data){
          this.company = data.company.company_name;
          this.address = data.company.address;
          this.city = data.company.city;
          this.state = data.company.state;
          this.postal_code = data.company.postal_code;
          this.phone = data.company.phone;
          this.description = data.company.description;
          this.tagline = data.company.tagline;     
        }
      },
      error =>{
        console.log(error);
      }
    );
  }
  
}
