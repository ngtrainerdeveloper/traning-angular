import { Component, OnInit } from '@angular/core';
import { ProviderClass } from '../model/provider.model';
import { providers } from '../model/providers.data';
import { ProviderService } from '../services/providers.service';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styles: [
  ]
})
export class ProvidersComponent implements OnInit{

  providers: ProviderClass[] = [];

  constructor(private providerService: ProviderService){};

  ngOnInit(): void{
    this.providerService.getProviders().subscribe(
      (providers: ProviderClass[]) => {
        this.providers = providers;
      },
      (error) => console.log(error)
    );
  }

  //Get all the data from  MySQL
  loadData(){
    this.providerService.getProviders()
    .subscribe(
      data => {
        this.providers = data;
    },
    error => {
      console.log(error);
    });
  }
}
