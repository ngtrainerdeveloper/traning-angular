import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProviderClass } from 'src/app/model/provider.model';
import { providers } from 'src/app/model/providers.data';
import { ProviderService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-edit-providers',
  templateUrl: './edit-providers.component.html',
  styles: [
  ]
})
export class EditProvidersComponent {
  submitted = false;
  emailError = false;
  emailMsg = "Invalid email. Try again or contact us."
  providers!: ProviderClass[];
  provider = new ProviderClass();
  providersForm!: FormGroup;

 

  id!: number; //Service provider Id number from the url
  email!: string; //Service provider email from the URL
  providerLoaded = false;//load form only when data present
  constructor(private providerService: ProviderService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit(): void {
    this.buildFormControls();
    this.loadData();
    
    this.viewOnlyEditPage();
  }

  //Data in Edit page
  viewOnlyEditPage(){
    this.route.paramMap.subscribe(params => this.id = parseInt(params.get('id')!)); // Use "+" instead of "parseInt" for type conversion

    this.providerService.getProviderById(this.id)
    .subscribe(
      data =>{
        this.provider = data as ProviderClass;
        console.log(data);
        if(data){
          this.providersForm.setValue({
            firstname: this.provider.firstname,
            lastname: this.provider.lastname,
            position: this.provider.position,
            company_name: this.provider.company.company_name,
            address: this.provider.company.address,
            city: this.provider.company.city,
            state: this.provider.company.state,
            postal_code: this.provider.company.postal_code,
            phone: this.provider.company.phone,
            email: this.provider.company.email,
            description: this.provider.company.description,
            tagline: this.provider.company.tagline,
          });          
        }
        
        this.onProviderLoaded;
      },
      error =>{
        console.log(error);
      }
    )
  }

  //Access Control
  get acl(){return this.providersForm.controls;};

  handleSubmit() {
    console.log(this.providersForm.value);
  
    if (this.isInValidEmail()) {
      this.provider.firstname = this.providersForm.get('firstname')?.value;
      this.provider.lastname = this.providersForm.get('lastname')?.value;
      this.provider.position = this.providersForm.get('position')?.value;
      this.provider.company.company_name = this.providersForm.get('company_name')?.value;
      this.provider.company.address = this.providersForm.get('address')?.value;
      this.provider.company.city = this.providersForm.get('city')?.value;
      this.provider.company.state = this.providersForm.get('state')?.value;
      this.provider.company.postal_code = this.providersForm.get('postal_code')?.value;
      this.provider.company.phone = this.providersForm.get('phone')?.value;
      this.provider.company.email = this.providersForm.get('email')?.value;
      this.provider.company.description = this.providersForm.get('description')?.value;
      this.provider.company.tagline = this.providersForm.get('tagline')?.value;
  
      this.providerService.updateProvider(this.id, this.provider).subscribe(
        data => {
          this.submitted = true;
          this.emailError = false;
          setTimeout(() => {
            this.router.navigate(['/providers']);
          }, 1000);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.submitted = false;
    }
  }
  
  

  //Get all the data from MySQL
loadData() {
  this.providerService.getProviders().subscribe(
  data => {
  this.providers = data;
  this.onProviderLoaded;
  },
  error => {
  console.log(error);
  }
  );
  }

  //Loaded
  get onProviderLoaded(){
    return this.providerLoaded = true;
  }

  //Check for duplicate Email
  isInValidEmail(){
    let email = this.providersForm.get('email')?.value;
  
    if (this.providers.find((el) => el.company.email === email)) {
      this.emailError = true;
      return true;
    } 
      return false;
  }

  //Build form controls
  buildFormControls(){
    this.providersForm = new FormGroup({
      firstname: new FormControl("",[Validators.required, Validators.minLength(2)]),
      lastname: new FormControl("",[Validators.required, Validators.minLength(2)]),
      position: new FormControl(''),
      company_name: new FormControl(''),
      address: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(''),
      postal_code: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]{6}(-[0-9]{4})?')
      ]),
      phone: new FormControl('',[Validators.required, Validators.minLength(10), Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]),
      email: new FormControl('',[Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      description: new FormControl(''),
      tagline: new FormControl(''),
    });

    
  }
}
