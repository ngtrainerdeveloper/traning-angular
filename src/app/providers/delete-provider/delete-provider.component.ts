import { ActivatedRoute, Router } from '@angular/router';
import { ProviderService } from './../../services/providers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-provider',
  templateUrl: './delete-provider.component.html',
  styles: []
})
export class DeleteProviderComponent implements OnInit {
  id: number = 0;
  fullname: string = '';

  constructor(
    private providerService: ProviderService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.deleteRecord;
  }

  get deleteRecord(){
    this.route.paramMap.subscribe(params => this.id = parseInt(params.get('id')!));
    this.providerService.getProviderById(this.id).subscribe(
      data =>{
        this.fullname = data.firstname + " "+ data.lastname;
      },
      error =>{
        console.log(error);
      }
    );

    return this.providerService.deleteProviderById(this.id).subscribe(
      (data) => {
        console.log(data);
        
         if(data == null){
          setTimeout(() => {
            this.router.navigate(['/providers']);
          }, 2000);
         }
        
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
