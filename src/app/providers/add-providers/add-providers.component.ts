import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProviderService } from 'src/app/services/providers.service';
import { ProviderClass } from '../../model/provider.model';
import { providers } from '../../model/providers.data';

@Component({
  selector: 'app-add-providers',
  templateUrl: './add-providers.component.html',
  styles: [
  ]
})
export class AddProvidersComponent implements OnInit {
  submitted = false;
  emailError = false;
  emailMsg = "Invalid email. Try again or contact us."
  providers!: ProviderClass[];
  provider = new ProviderClass();
  providersForm!: FormGroup;
  constructor(private providerService: ProviderService, private router: Router) {

  }

  ngOnInit(): void {
    this.buildFormControls();
    this.loadData();
  }

  

  //Access Control
  get acl(){return this.providersForm.controls;};

  handleSubmit() {
    console.log(this.providersForm.value);
    
    if (!this.isInValidEmail()) {
      this.buildNewProvider();
      this.providerService.createProvider(this.provider).subscribe(
        data => {
          this.submitted = true;
          this.emailError = false;
            setTimeout(() => {
              this.router.navigate(['/providers']);
            }, 900);
        },
        error => {
          console.log(error);
        }
      );
    } else {
      this.submitted = false;
      // this.emailError = true;
    }
  }

  //Get all the data from MySQL
loadData() {
  this.providerService.getProviders().subscribe(
  data => {
  this.providers = data;
  },
  error => {
  console.log(error);
  }
  );
  }

  //Check for duplicate Email
  isInValidEmail(){
    let email = this.providersForm.get('email')?.value;

    if (this.providers.find((el) => el.company.email === email)) {
      this.emailError = true;
      return true;
    } 
      return false;
  }

  //Build New Provider
  buildNewProvider(){
    let p = this.providersForm.value;
    this.provider.id = this.genNewId();
    this.provider.firstname = p.firstname,
      this.provider.lastname = p.lastname,
      this.provider.position = p.position,
      this.provider.company = {
        company_name : p.company_name,
        address : p.address,
        city : p.city,
        state : p.state,
        postal_code : p.postal_code,
        phone : p.phone,
        email : p.email,
        description : p.description,
        tagline : p.tagline,
      };
  }

  //Generate New Id
  genNewId(){
    let newId: number;
    while (true) {
      newId = Math.floor(Math.random() * 10000) + 99999;
      if (providers.findIndex((el: { id: number; }) => el.id == newId) == -1) {
        return newId;
      }
    }
  }

  //Build form controls
  buildFormControls(){
    this.providersForm = new FormGroup({
      firstname: new FormControl("",[Validators.required, Validators.minLength(2)]),
      lastname: new FormControl("",[Validators.required, Validators.minLength(2)]),
      position: new FormControl(),
      company_name: new FormControl(),
      address: new FormControl(),
      city: new FormControl(),
      state: new FormControl(),
      postal_code: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]{6}(-[0-9]{4})?')
      ]),
      phone: new FormControl("",[Validators.required, Validators.minLength(10), Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]),
      email: new FormControl("",[Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      description: new FormControl(),
      tagline: new FormControl(),
    });

    
  }
}
