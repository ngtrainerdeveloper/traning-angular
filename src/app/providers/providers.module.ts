import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProvidersComponent } from './providers.component';
import { AddProvidersComponent } from './add-providers/add-providers.component';
import { DetailsProvidersComponent } from './details-providers/details-providers.component';
import { EditProvidersComponent } from './edit-providers/edit-providers.component';
import { DeleteProviderComponent } from './delete-provider/delete-provider.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [ProvidersComponent, AddProvidersComponent, DetailsProvidersComponent, EditProvidersComponent, DeleteProviderComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: [ProvidersComponent, ReactiveFormsModule, AddProvidersComponent, DetailsProvidersComponent, EditProvidersComponent, DeleteProviderComponent],
  providers: []
})
export class ProvidersModule { }
